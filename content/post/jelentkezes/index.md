---
title: "Jelentkezés"
date: "2020-11-14"
description: "Igényeljen segítőkutyát, terápiát vagy oktatást tőlünk"
category: "Jelentkezés"
tags:
  - jelentkezes
---

* [Segítőkutya igénylés](/jelentkezes/segitokutya-igenyles/)
* [Képzések](/jelentkezes/kepzes/)
* Önkéntesnek

<div class="alert alert-primary" role="alert"><div class="font-weight-bold">
Tekintettel a helyzetre, arra kérjük a képzés és önkéntes
munka iránt érdeklődőket, hogy írásban vagy 
telefonon <a href="/elerhetoseg/">keressenek meg</a> minket.
</div></div>
