---
title: "Milyen helyekre vihetem be a vizsgázott segítő kutyámat?"
date: "2020-05-21"
description: "A 27/2009. (XII. 3.) SZMM rendelet szabályozza."
category: "GYIK"
tags:
  - gyik
---

27/2009. (XII. 3.) SZMM rendelet
a segítő kutya kiképzésének, vizsgáztatásának és alkalmazhatóságának szabályairól értelmében: 9. § (1) A Fot. 7/A. § (1) bekezdésében meghatározott, a közszolgáltatásokhoz való egyenlő esélyű hozzáférés biztosítása érdekében a gazda és a kiképző (a továbbiakban együtt: segítő kutyát alkalmazó személy) jogosult a - (2) bekezdés szerinti kivétellel - segítő kutyával tartózkodni és a segítő kutyát használni a lakosság elől elzárt területek kivételével a Fot. 4. § f) pontja szerinti közszolgáltatást nyújtó szerv, intézmény, szolgáltató területén és egyéb, mindenki számára nyitva álló létesítményben (területen), így különösen

a) közforgalmú tömegközlekedési eszközön,

b) üzletben, ideértve az élelmiszert árusító üzletet és a vendéglátó üzletet is,

c) bevásárlóközpontban,

d) piacon,

e) vásáron,

f) szálláshelyen,

g) játszótéren,

h) \* egészségügyi, közművelődési, oktatási, szociális, gyermekjóléti, gyermekvédelmi intézményben,

i) közfürdő területén,

j) állatkertben,

k) közterületen.

(2) Terápiás kutyával tartózkodni és terápiás kutyát használni a lakosság elől elzárt területek kivételével az (1) bekezdés h) pontja szerinti intézményben lehet.

(2a) \* A 4. § (1) bekezdés d) pontja szerinti tanúsítvánnyal rendelkező személy jogosult arra, hogy a kiképzés alatt álló kutyával kiképzés céljából az (1) bekezdés szerinti létesítményben tartózkodjon.

(3) Mozgólépcsővel felszerelt létesítményben (területen) a segítő kutyát alkalmazó személy jogosult a segítő kutyával az üzemképes, álló mozgólépcsőt használni.

(4) A létesítmény (terület) fenntartója, üzemeltetője, tulajdonosa, illetve az általa megbízott személy a segítő kutyát akkor távolíthatja el a létesítményből (területről), ha a segítő kutya a létesítményben (területen) tartózkodók testi épségét veszélyeztető magatartást tanúsít.
