import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

const DeFlag = () => {
  const data = useStaticQuery(graphql`
    query {
      logo: file(relativePath: { eq: "DE.png" }) {
        childImageSharp {
          fixed(height: 28) {
            ...GatsbyImageSharpFixed_withWebp
          }
        }
      }
    }
  `)

  return <Img fixed={data.logo.childImageSharp.fixed} />
}

export default DeFlag
