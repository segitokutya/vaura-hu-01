import React from "react"
import { graphql } from "gatsby"
import { Container } from "react-bootstrap"

import Layout from "../components/layout"
import PostsList from "../components/postlist"
import Slider from "../components/slider"

const CategoryTemplate = ({ location, pageContext, data }) => {
  const { tag } = pageContext

  const title = `Címke '${tag}'`
  const subtitle = "Minden cikk az adott címkével"

  return (
    <Layout location={location} title={title} description={subtitle}>

      <Slider title={title} subtitle={subtitle}/>

      <Container fluid>
        <PostsList postEdges={data.allMarkdownRemark.edges}/>
      </Container>
    </Layout>
  )
}

export const pageQuery = graphql`
  query TagPage($tag: String) {
    allMarkdownRemark(
      limit: 1000
      filter: { fields: { tags: { in: [$tag] } } }
    ) {
      totalCount
      edges {
        node {
          fields {
            slug
            tags
          }
          excerpt
          timeToRead
          frontmatter {
            title
            description
            date
          }
        }
      }
    }
  }
`

export default CategoryTemplate
