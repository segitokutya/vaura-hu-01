---
title: "Service Dog Hungarian Regulation"
date: "2020-06-13"
description: "English translation of Regulation Nr. 27/2009 (XII.3.) of the Ministry of Social Affairs and Labor."
category: "Tudástár"
tags:
  - tudastar
  - rendeletek
  - english
---

_**The Regulation up to date version [may be found here](https://net.jogtar.hu/jogszabaly?docid=A0900027.SMM).
This English translation is for informative purposes only, and is not a legally definitive translation of the
original Regulation. We do not take any liability for eventual mistakes of any kind present in this translation.**_

Regulation Nr. 27/2009 (XII.3.) of the Ministry of Social Affairs and Labor on the rules of training, conducting the examinations and the applicability of assistance dogs established by the Minister of Labor and Social Affairs

On the basis of the authorization granted by Subsection (2) of Section 30 of Act XXVI of 1998 on the Rights and Equal Opportunities for Persons with Disabilities, acting in my role set out in Subsection h) of Section 1 of Government Regulation Nr. 170/2006 (VII.28.) on the Duties and Powers of the Minister of Social Affairs and Labor, it is hereby decided that:

#### 1. §

(1) For the purpose of this Regulation,

a) persons with disabilities: persons referred to in Subsection a) of Section 4 of Act XXVI of 1998 on the Rights and Equal Opportunities for Persons with Disabilities and the Convention on the Rights of Persons with Disabilities and Subsection 1 of the related Act XCII on the promulgation of the Optional Protocol,

b) assistance dogs:  dogs providing habilitation and rehabilitation tasks satisfying the animal health requirements set out in separate legislation, assisting persons with disabilities in exercising their right to equal access, facilitating their independent lifestyle and  in emergency relief,

c) owner:

* ca) a disabled person who uses an assistant dog and attends a training on the use and care of an assistant dog and passes the related exam,

* cb) a person controlling a therapy dog, who attended a training on the use and care of assistant dogs and passed the related exam,

d) public land: a term specified in Subsection 13 of Section 2 of Act LXXVIII of 1997 on the shaping and conservation of built environment,

e) shopping center: a term specified in Subsection 3 of Section 2 of Act CLXIV of 2005 on trade (hereinafter referred to as Trade Act),

f) market: a term specified in Subsection 19 of Section 2 of the Trade Act,

g) accommodation: a term specified in Subsection 22 of Section 2 of the Trade Act,

h) Shop: a term specified in Subsection 27 of Section 2 of the Trade Act,

i) fair: a term specified in Subsection 29 of Section 2 of the Trade Act,

j) zoo: a facility specified in Subsection 3 of Section 3 of Act XXVIII of 1998 on the protection and welfare of animals (hereinafter referred to as PWA Act).

#### 2.§

(1) The type of assistant dog according to his training and applicability

a) guide dog: a dog trained to guide a visually impaired person,

b) dog assisting a person with reduced mobility (PRM): a dog trained to assist a person with reduced mobility in their everyday activities,

c) hearing dog: a dog trained to alert a hearing-impaired person to a danger or other important sounds,

d) seizure response dog: a dog trained to provide assistance to a person living with epilepsy or other persons at risk of chronic conditions like seizures,

e) personal assistance dog: a dog trained to help a disabled person to live an independent life,

f) therapy dog: a dog used in pedagogical, psychological, psychiatric and conductive pedagogical habilitation and rehabilitation processes in special education and social services.

(2) Assistance dogs should be specially marked on which the logo of the organization which has trained the dog in accordance with Subsection (1) of Section 3 shall be indicated.

(3) If the therapy dog is not used by the organization that has trained the dog, the dog must wear a distinctive mark with the logo of the organization that uses the dog.

#### 3. §

(1) An organization (hereinafter referred to as dog training organization) may train an assistance dog and prepare people for the use and care of an assistance dog

a) if in its Deed of Company Formation the training of assistance dogs and giving them to people with disabilities is mentioned, and

b) which ensures that the training is performed by a person (hereinafter referred to as the trainer) who has a habilitation dog trainer qualification listed in the National Qualification Register.

(2) An organization using a therapy dog is a dog training organization which employs a therapy dog trained and examined by them or another dog training organization and the dog’s owner.

#### 4. §

(1) The minister responsible for social and pension policy appoints an intermediate body from among dog training organizations and organizations that improve the use of assistance dogs, which

a) publishes on its website

* aa) the list of dog training organizations for information,

* ab) the requirements specified in Subsection (4) of Section 5,

b) contributes to the work of the dog training organization with technical advice,

c) ensures the conduct of the examination specified in Subsection (1) of Section 5 in accordance with the rules laid down in the law and issues a certificate of the successful exams,

d) issues an assistance dog trainer certificate with photo for the trainers of dog training organizations.

(2) For the activities referred to in Subsection (1), the intermediate body may request the training organization to cover their administrative costs incurred in connection with the organization of the examination (the fee of the administrator organizing the exam, telephone and postage, the production costs of the examination papers, the cost of issuing the certificate), which shall not exceed 20% of the prevailing minimum old-age pension per dog registered for exam.

(3) The designation of the intermediate body is for five years. The same organization may be designated several times as intermediate body.

#### 5. §

(1) The training of an assistance dog and the preparation of a person with disabilities for the use and care of an assistance dog is completed by an examination before an examining board, in accordance with an exam schedule set out in the Annex.

(2) The dog training organization provides for the organization of the exam and the existence of the personnel and material conditions for the conduct of the examination.

(3) The amount of examination fee to be paid by the owner of the dog is determined by the dog training organization, in that, it shall not exceed 20% of the prevailing minimum old-age pension.

(4) The owner and the assistance dog must meet the requirements developed by the intermediate body and specified in the regulation approved by the minister responsible for social and pension policy.

#### 6. §

(1) The examining board shall consists of

a) two members delegated by the intermediate body ,

b) a member delegated by the organization that has trained the dog,

c) – if requested by the disabled person or a legal representative – a member indicated by the foregoing and delegated by an interest organization working in a specific field relevant to the handicap or disease of the person with a disability.

(2) The member of the examining board under Subsection a) and b) of Section (1) shall have a habilitation dog trainer qualification listed in the National Qualification Registry and professional experience of at least 5 years in training and examining assistance dogs.

(3) The members delegated by the intermediate body or an interest organization or their relatives may not be members or employees of the organization that has trained the dog.

(4) The member delegated by the organization that has trained the dog may not be the trainer or raiser of the dog to be examined.

(5) The intermediate body and the interest organization shall notify in writing the organization that has trained the dog of the member(s) they delegated within 15 days of receipt of the invitation of the organization that has trained the dog. The intermediate body shall state in the notice as to who the chairman of the examining board will be from the members they delegated.

#### 7. §

(1) After seeking the opinion of the examining board members, the dog training organization determines the exam date and venue, and convenes the examining board for the purpose of conducting the examination.

(2) The exam venue shall be determined on the basis of the rules provided for in Subsection (4) of Section 5 in a manner that it shall correspond to all conditions of the use of the would-be assistance dog.

(3) Members of the examining board must be notified in writing of the convention of the board no later than 15 days prior to the exam date. At the same time, the intermediate body shall publish on their website

a) the exam venue,

b) the exam date,

c) the names of the members of the examining board, subject to their consent,

d) the name of the assistance dog registered for the exam,

e) the name of the trainer of the assistance dog, subject to his/her consent, and

f) the name of the owner of the assistance dog, subject to his/her consent.

(4) The candidate and his/her legal representative must be notified in writing of the exam date and venue, as well as the examining board members no later than 15 days prior to the date of the exam. In the notification the attention of the candidate and his/her legal representative must be drawn on the conditions for the commencement and smooth run of the examination, as well as on the possibility of having recourse to an interpreter or a sign language interpreter.

(5) The intermediate body shall issue a certificate for the owner of the dog after passing the exam.

#### 8. §

(1) The dog training organization and the owner conclude a written contract of the transfer of the assistance dog, including

a) the legal title of the transfer (e.g. buying and selling, provided for use),

b) the name, registered seat and contact information of the dog training organization,

c) the responsibility of the dog training organization that the assistance dog when transferred meets the animal health requirements set out in separate legislation,

d) the contractual rights and obligations of the dog training organization and the owner, in particular the itemized monetary obligations incumbent on the owner,

e) the possibility of the assistance dog to be returned to the dog training organization, its causes or the exclusion of returning the assistance dog.

(2) When handing over the assistance dog, the dog training organization informs the owner of the deadline for the enforcement of warranty claims.

#### 9. §

(1) In order to ensure equal access to public services, as defined in Subsection (1) of Section 7/A of the Act on the Rights and Equal Opportunities for Persons with Disabilities, the owner and the trainer (hereinafter together referred to as a person using an assistance dog) are entitled to – except under Subsection (2) – stay and use an assistance dog in the territory of a body, institution, service provider providing public services according to Subsection f) of Section 4 of the Act on the Rights and Equal Opportunities for Persons with Disabilities – except for areas to which the public do not have access – and other facilities (areas) open to all, in particular in

a) public vehicles,

b) stores, including stores selling groceries and catering business, too,

c) shopping centers,

d) markets,

e) fairs,

f) accommodations,

g) playgrounds,

h) institutions of public culture, education, social services, child welfare and child protection,

i) public baths,

j) zoos,

k) public places.

(2) It is possible to stay and use an assistance dog in institutions specified in point h) of Subsection (1), except for areas to which the public do not have access.

(2a) A person having a certificate in accordance with point d) of Subsection (1) of Section 4 is entitled to stay with a dog in training in facilities specified in Subsection (1) for the purpose of training.

(3) In facilities (areas) with escalator the person using an assistance dog is entitled to use the serviceable, stationary escalator.

(4) The maintainer, operator, owner of the facility (area) or the person appointed by them may remove the assistance dog from the facility (area), if the assistance dog’s behavior gives rise to a risk to the body or life of those in the facility (area).

#### 10. §

(1) The requirements on the number and size of dogs specified in the regulation of the local government for keeping dogs shall not apply on assistance dogs used or withdrawn.

(2) The person using an assistance dog may use the assistance dog without a muzzle.

(3) The intermediate body in accordance of Subsection (1) of Section 4 issues a separate certificate for withdrawn assistance dogs, in which the fact of the dog’s withdrawal is recorded.

#### 11. §

6 Sections 9 and 10 shall be applied to assistance dogs already trained and brought to Hungary and to the dog’s owner, provided that the assistance dog and the dog’s owner has a clearly identifiable document, which was issued by a member organization of the European Guide Dog Federation (EGDF) or the International Guide Dog Federation (IGDF) in case of guide dogs or the Assistance Dogs Europe (ADEU) or the Assistance Dogs International (ADI) in case of other assistance dogs.

#### 12. §

(1) The ownership and protection of assistance dogs are governed by the provisions of the PWA Act, in that,

a) – in the absence of a contrary agreement between the owner and the dog training organization – the organization that trained the dog shall take care of the withdrawn assistance dog,

b) the trainer and after the assistance dog has been handed over, the owner is considered to be the keeper of the dog.

(2) The responsibility for the damage caused by the assistance dog shall be governed by the rules of Act V of 2013 on the Civil Code concerning the responsibility of keepers of animals, in that, during the training of the assistance dog, the trainer and after the assistance dog has been handed over, the owner is considered to be the keeper of the dog.

(3) The owner and the organization that has trained the dog have the right to act in connection with the offence committed against the assistance dog.

#### 13. §

(1) This Regulation – with the exceptions as specified in Section 2 – shall enter into force eight days after its publication.

(2) Subsection (2) of Section 6 of this Regulation shall enter into force on 1 January 2017.

(3) The provisions specified in point b) of Subsection (1) of Section 3 of this Regulation shall apply to organizations training assistance dogs on the date of entry into force of this Regulation from 1 January 2012.

(4) Sections 9 and 10 of this Regulation shall apply to a dog trained to be an assistance dog before the entry into force of this Regulation and the dog’s owner – with the exception set out in Subsection (5) – without taking the exam in accordance with Subsection (1) of Section 5.

(5) A dog trained to be a therapy dog prior to the entry into force of this Regulation may be used as a therapy dog after 31 December 2010 after passing the examination specified in Subsection (1) of Section 5.

#### 14. § 

(1) This Regulation shall serve to meet the provisions of Directive 2006/123/EC of the European Parliament and of the Council of 12 December 2006 on services in the internal market. Annex to Regulation 27/2009 (XII.3.) of the Ministry of Social Affairs and Labor.Schedule for the examination closing the training of the assistance dog and the preparation of the owner for the use and care of the assistance dog

## Addendum

#### I. The examining board’s activities

1. The examining board will check the existence of the material conditions for the exam within the framework of an initial meeting before the exam starts.

2. In case of examinations defined in respect of guide dogs and their owners, dogs assisting persons with reduced mobility and their owners, hearing digs and their owners, seizure response dogs and their owners, and personal assistance dogs and their owners and second partial exam defined in respect of therapy dogs and their owners a total of up to 6 people may take the exam, in case of the first partial exam in respect of therapy dogs and their owners a total of up to 10 people may take the exam. If the number of candidates exceeds the number specified above, several examining boards shall be compiled.

3. During the exam, the examining board ensures the regular and smooth conduct of the examination, and, if necessary, takes the measures referred to in points II/4-8.

4. After the completion of the examination, the examining board evaluates the performance of the candidates and the assistance dogs admitted to the exam within the framework of a closing meeting.

5. The members of the examining board are entitled to compensation for their travel expenses. Members of the examining board may be compensated by means of an honorarium in addition to the compensation for their travel expenses.

6. If a member of the examining board forgoes the compensation for the travel expenses and the honorarium, this fact must be recorded in the exam report.

#### II. Conditions for the commencement and smooth conduct of the exam

1. Before the start of the exam the candidate presents his/her identifiable personal document with photo. The candidate dog’s unique identification is done by reading the dog’s chip code.

2. The candidate turning up for the exam in an unsuitable state (e.g. under the influence of alcohol, apparently dazed) may not start the exam.

3. It is not allowed to take and use turned on mobile phones, tape recorder, CD player, devices for storing images and/or texts, other electronic or printed tools  during the exam (unless checked by the examining board and needed for the exam).

4. Candidates without an identifiable personal document with photo, being in unsuitable conditions or taking and using unpermitted tools specified in section 3 during the exam will be disqualified from the exam.

5. The exam of a candidate not turning up by the scheduled time of the exam is considered to be unsuccessful, unless the candidate submits documents as proof of his/her absence (medical certificate, accident report, etc) within 3 days.

6. The chairman of the examining board decides on the acceptance of the certificates of absence. Upon acceptance of the certificate, the candidate may take the examination without paying the examination fee again.

7. During the time of the exam candidates may leave the exam venue only in exceptional cases (e.g. health reasons). If a candidate e unduly leaves the exam venue, the examining board will interrupt the exam and evaluate the candidate’s personal performance based on the candidate’s performance up to then.

8. The exam can be interrupted, divided or taken on several occasions only when justified, particularly under unsuitable weather conditions, in case of an accident or for health reasons. In this case, the partial results must be recorded on the grade sheet, and if the exam cannot be continued that day or the composition of the board changes, a new column must be filled in on the grade sheet, and the reason for and the results of the interrupted exam must also be indicated thereon.

9. The fact and the circumstances of the disqualification from and the interruption of the exam shall be recorded.

#### III. Use of an interpreter or a sign language interpreter

1. At the exam the candidate may request the contribution of an interpreter or a sign language interpreter.

2. The interpreter and the sign language interpreter cannot be the person who has trained the dog.

3. The contribution of the interpreter or the sign language interpreter shall be recorded in the examination report and he/she should be required before the exam starts to collaborate and translate in an objective and trustworthy way.

4. If the interpreter or sign language interpreter does not participate in the examination in accordance with the professional rules, the examining board interrupts the exam and excludes the interpreter or sign language interpreter from participation in the exam and instructs the candidate to repeat the exam.

#### IV. Documentation of the exam

1. The organization that has trained the dog shall draw up a report of the exam.

2. The list of candidates recorded in the exam report drawn up by the examining board should be closed no later than 5 working days before the exam. The exam cannot be started without closing the list. The exam report shall be maintained for 10 years.

3. The exam report includes:

    1. the exam date,
    2. the exam venue,
    3. the type of the exam, the name of the qualification at which the exam is aimed,
    4. the name of the exam subjects,
    5. the names of the candidates and their personal identification subject to their consent,
    6. the names of the members of the examining boars,
    7. the name of the body that has trained the dog,
    8. the discussions that took place at the initial and final meetings,
    9. the fact of the contribution of an interpreter or sign language interpreter,
    10. the fact of the interruption of or disqualification from the exam,
    11. the evaluation of the assistance dogs and the candidates, the exam result
    12. any circumstances affecting the course of the exam.

4. A re-take exam shall be included in a separate report.

5. The tasks from which the candidate is exempted must be marked with an “(FM)” symbol in the exam report.

6. The exam report is signed by the members of the examining board and the person who drew it up.

7. The organization that has trained the dog records the entire exam which is evaluated by the examining board, of which the candidate is informed.

8. The organization that has trained the dog shall send a copy of the exam report in electronic format and an unedited copy of the recording in playable format to the intermediate body within 15 days after the exam.

9. The organization that has trained the dog and the intermediate body shall keep the recording for a year following the withdrawal of the assistance dog from service.

10. In addition to the examining board, only the candidate and the experts designated by the intermediate body can watch the recording.

11. The candidate can submit his/her complaints regarding the proper conduct of the examination within 15 days after the exam in writing to the intermediate body. The intermediate body – involving experts if necessary – mediates in solving the problem by settling the dispute between the candidate and the examining body and the candidate and the organization that has trained the dog.

12. A person may be appointed as an expert who

    1. has a qualification specified in point b) of Subsection (1) of Section 3,
    2. has at least 5 years of experience in training and conducting examinations, corresponding to the type of dog admitted to the exam that gave rise to the complaint,
    3. did not participate either in the organization and conduct of the exam that gave rise to the complaint or the training of the dog admitted to the exam.
    4. is not a member of the board of the intermediate body and does not hold any office in the intermediate body.

13. The designated expert’s fee is paid by the party lodging the complaint.
