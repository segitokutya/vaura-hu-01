---
title: "Vakvezető kutya"
date: "2020-06-20"
description: "A látássérült személy vezetésére kiképzett kutya."
category: "Tudástár"
tags:
  - tudastar
---

A látássérült személy vezetésére kiképzett kutya, akinek elsődleges feladata 
gazdájának biztonságos vezetése a közúti forgalomban. Megállással jelezni az 
akadályokat, illetve biztonságosan kikerülni azokat. Ezen kívül a látássérült 
segítő kutya feladata meghatározott objektumokat, például gyalogátkelőt, 
lépcsőt, ajtót, buszmegállót, ülőhelyet, stb. megkeresni és jelezni a gazdának. 

A vakvezető kutyától nem várható el, hogy elvezesse a gazdát „A” pontból „B” 
pontba. A gazda feladata megtanulni tájékozódni és a megfelelő vezényszavakkal 
irányítani a kutyát. A kutya nem tesz mást, mint hogy akadálytól akadályig 
biztonságosan elvezeti őt. Ugyanígy nem várható el tőle, hogy a jelzőlámpa színe 
alapján döntsön az átkelés pillanatáról, mert színlátásuk nem teljesen bizonyított. 
A gazdának kell tudni, hogy mikor lehet megkezdeni az úttesten való átkelést a 
forgalom és / vagy a hangjelzést adó jelzőlámpa hangját figyelve, esetleg 
járókelőktől kérve segítséget. 

Vészhelyzetben a vakvezető kutya „értelmes engedetlenséget” 
mutathat, vagyis szükség esetén felülbírálja a gazda döntését, ezzel megvédve annak 
testi épségét. (MVGYOSZ, 2019) Ezt a fajta együttműködést, ami a látássérült és kutyája 
között is működik, komplementer kooperációnak nevezzük, lényege pedig a döntéshozó 
fél folyamatos váltogatása, azaz felváltva kezdeményezik az akció következő elemét a 
kutya és a gazda. Az efféle egymást kiegészítő jellegű együttműködés mindkét
fél aktív részvételét igénylő kooperatív tevékenység, melyet az irányítás 
(dominancia) folyamatos átadás-átvétele és a magas szintű interaktivitás 
jellemez, ami nagyon hasonló az emberek egymás közötti együttműködéséhez. 
(Köböl, Hevesi, és Topál, 2013)

#### Hivatkozások:

1. Magyar Vakok és Gyengénlátók Országos Szövetsége honlapja (MVGYOSZ): http://www.mvgyosz.hu
2. Köböl Erika, Hevesi Tímea Mária és Topál József (2013): Állatasszisztált foglalkozások.
   „Mentor(h)áló 2.0 Program” TÁMOP-4.1.2.B.2-13/1-2013-0008 projekt. On-line megtekintés:
   http://www.jgypk.hu/mentorhalo/tananyag/Allatasszisztalt_foglalkozasV2/index.html
