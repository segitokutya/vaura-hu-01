import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

const EnFlag = () => {
  const data = useStaticQuery(graphql`
    query {
      logo: file(relativePath: { eq: "GB.png" }) {
        childImageSharp {
          fixed(height: 28) {
            ...GatsbyImageSharpFixed_withWebp
          }
        }
      }
    }
  `)

  return <Img fixed={data.logo.childImageSharp.fixed} />
}

export default EnFlag
