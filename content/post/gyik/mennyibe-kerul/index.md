---
title: "Mennyibe kerül egy segítőkutya kiképzése?"
date: "2020-07-14"
description: "A kutya képzésének költsége annak típusától függ."
category: "GYIK"
tags:
  - gyik
---

Nagyságrendileg: terápiás 1,5 MFt, személyi segítő 2,3-2,6 MFt, mozgássérültet 
segítő 2,5-3 MFt, rohamjelző 2,5-3,5 MFt.
