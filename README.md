# VAURA új honlapja

## Fejlesztése

Kell hozzá:

- node (és npm de jön vele)
- git
- gatsby-cli (globálisan `npm i -g gatsby-cli`)

Lépésenként:

1. a forrást checkoutold ki
2. `npm install`
3. `gatsby clean` (esetleg) majd `gatsby develop`
4. nyisd meg a http://localhost:8000 URL-t és ott a szájt
5. módosítsd a forrásokat
6. `git commit` majd `git push` és a netlify telepíti az frisset

Stage: https://vaura.netlify.com/
Final: https://vaura.hu/

## Jegyzetek

