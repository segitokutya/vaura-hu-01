---
title: "Frissítve a honlap"
date: "2020-11-14"
author: "Tamás"
description: "Az AURA önkénteseinek oldala frissítve."
category: "Hírek"
tags:
  - hirek
  - onkentes
---

Frissült a honlap, az AURA csapata további tagjai is
fellelhetőek a [honlapunkon](/rolunk/tagok/)!
