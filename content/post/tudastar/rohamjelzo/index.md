---
title: "Rohamjelző kutya"
date: "2020-06-20"
description: "Az epilepsziával élő személy vagy más krónikus, rohamszerű állapotoktól veszélyeztetett személy segítésére kiképzett kutya."
category: "Tudástár"
tags:
  - tudastar
---

Az epilepsziával élő személy vagy más krónikus, rohamszerű állapotoktól 
veszélyeztetett személy segítésére kiképzett kutya. Elsősorban a váratlanul bekövetkező
rohamok vagy rohamszerű állapotok előrejelzése a feladatuk, illetve a roham közbeni 
segítségnyújtás, segítségkérés. 

Itt még a hangjelző kutyáknál is fontosabb a rendkívül szoros 
kötődés, ez biztosítja ugyanis, hogy a kutya maradéktalanul a gazdára fókuszáljon, ismerje annak
minden rezdülését, megnyilvánulását, reakcióját pszichés és fizikai fiziológiás értelemben
egyaránt. 

A kutyák tulajdonképpen a szervezetben lejátszódó kémiai folyamatokra reagálnak,
és így tudják jelezni a rohamot. Ennek elérése érdekében ezek a kutyák kb. nyolchetesen a
gazdához kerülnek, és a képzés is együtt történik. A családnak itt egyebek közt azt is vállalnia
kell, hogy a gazdán kívül a kutyát senki sem simogathatja, utasíthatja vagy etetheti, amiből az
következik, hogy a gazdának képesnek kell lennie a kutya önálló ellátására. A kutya a 
rohamokat előre jelzi, így a gazdának lehetősége van biztonságba helyezni magát a roham előtt,
illetve időben be tudja venni gyógyszerét, ezzel megelőzve magát a rohamot. Amennyiben a
roham mégis bekövetkezik, és szükséges, a kutya képes segítséget hívni. 

A tapasztalatok azt
mutatják, hogy a rohamjelző kutyával rendelkező betegek rohamai ritkulnak, lefolyásuk
enyhébb, rövidebb. Arra is volt példa, hogy a beteg idővel tünetmentessé vált. Az ellátandó 
feladat komplexitása miatt minden száz kölyökből csak egy alkalmas erre a feladatra, a kiképzés
pedig évekig tart. (KEA, 2019)


#### Hivatkozások:

1. KEA Kutyával az Emberért Alapítvány honlapja, On-line megtekintés: http://www.kea-net.hu/?page=epilepszia_roham-jelzo_kutyak
