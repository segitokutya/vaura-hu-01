---
title: "Boldog szülinapot Ozzy!"
date: "2020-05-18"
author: "Tamás"
description: "Három éves Ozzy"
category: "Hírek"
tags:
  - hirek
  - ozzy
---

A mai napon töltötte be harmadik életévét Ozzy, a személyisegítő-kutya.

![Ozzy](20200418_101926.jpg)
