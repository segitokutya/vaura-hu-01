---
title: "About us"
description: "All about AURA Foundation"
date: "2020-11-25"
category: "Rólunk"
langKey: en
tags:
  - rolunk

---

Itt mindent megtudhat rólunk. A [történetünk](/rolunk/tortenetunk) itt van részletesen leírva.

- [AURA története](/rolunk/tortenetunk/)
- [Csapatunk](/rolunk/tagok/)
- [Terápiás helyszineink](/rolunk/terapias-helyszineink/)

#### Média

- [Médiamegjelenés](/rolunk/media/)
- [Videók](/rolunk/videok/)
